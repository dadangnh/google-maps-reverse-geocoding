# Google Reverse Geocoding - PHP

Script to crawl latitude and longitude coordinate from Google API.

## Canonical source

The canonical source of Google Reverse Geocoding where all development takes place is [hosted on GitLab.com](https://gitlab.com/dadangnh/google-maps-reverse-geocoding).

## Requirements

To use this tool, you need:
*  PHP Runtime | version 7.4.5 or newer is recommended.
*  Database server | we use [PostgreSQL 12](https://www.postgresql.org/), but you can use any databases supported by [Doctrine Project](https://www.doctrine-project.org/projects/doctrine-dbal/en/current/reference/introduction.html).
*  [Docker](https://docker.com/).
*  [Composer](https://getcomposer.org/download/).
*  [Symfony CLI](https://symfony.com/download) (Optional but recommended)
