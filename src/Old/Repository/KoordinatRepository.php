<?php

namespace App\Old\Repository;

use App\Old\Entity\Koordinat;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\DBAL\DBALException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Koordinat|null find($id, $lockMode = null, $lockVersion = null)
 * @method Koordinat|null findOneBy(array $criteria, array $orderBy = null)
 * @method Koordinat[]    findAll()
 * @method Koordinat[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class KoordinatRepository extends ServiceEntityRepository
{
    private $conn;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Koordinat::class);
        $this->conn = $this->getEntityManager()->getConnection();
    }

    /**
     * @return Koordinat[] Returns an array of Koordinat objects
     * @throws DBALException
     */
    public function findTotalData()
    {
        return $this->conn->query('select count(*) from koordinat')->fetch();
    }

    /**
     * @return Koordinat[] Returns an array of Koordinat objects
     * @throws DBALException
     */
    public function findRandomData($limit)
    {
        $stmt = $this->conn->prepare('select * from koordinat order by random() limit :limit');
        $stmt->execute([':limit' => $limit]);
        return $stmt->fetchAll();
    }

    public function getTotalUnmigratedData()
    {
        return $this->conn->query('select count(*) from koordinat where upgraded is not true')->fetch();
    }

    public function checkUpgradeColumnReady()
    {
        $columns = $this->conn->query("
            select column_name
            from information_schema.columns
            where table_schema = 'public'
              and table_name = 'koordinat'
        ")->fetchAll();

        foreach ($columns as $column) {
            if ('upgraded' === $column['column_name']) {
                return true;
            }
        }

        return false;
    }

    public function addUpgradeColumn()
    {
        return $this->conn->exec('alter table koordinat add upgraded boolean');
    }

    public function getListCanUpgrade(int $count)
    {
        $stmt = $this->conn->prepare('select * from koordinat where upgraded is not true limit :limit');
        $stmt->execute([':limit' => $count]);
        return $stmt->fetchAll();
    }

    public function saveUpgraded(int $id)
    {
        $stmt = $this->conn->prepare('update koordinat set upgraded = true where id = :id');
        if ($stmt->execute([':id' => $id])) {
            return true;
        }

        return false;
    }
}
