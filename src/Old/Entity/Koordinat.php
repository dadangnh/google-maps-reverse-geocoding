<?php

namespace App\Old\Entity;

use App\Old\Repository\KoordinatRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=KoordinatRepository::class)
 */
class Koordinat
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="float")
     */
    private $latitude;

    /**
     * @ORM\Column(type="float")
     */
    private $longitude;

    /**
     * @ORM\Column(type="geography", nullable=true)
     */
    private $geo;

    /**
     * @ORM\Column(type="json", nullable=true)
     */
    private $googleJson = [];

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $googleCode;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $streetNumber;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $route;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $adminLevel_5;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $adminLevel_4;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $adminLevel_3;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $adminLevel_2;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $adminLevel_1;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $country;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $postal;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $address;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $locationType;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $status;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $imported;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLatitude(): ?float
    {
        return $this->latitude;
    }

    public function setLatitude(float $latitude): self
    {
        $this->latitude = $latitude;

        return $this;
    }

    public function getLongitude(): ?float
    {
        return $this->longitude;
    }

    public function setLongitude(float $longitude): self
    {
        $this->longitude = $longitude;

        return $this;
    }

    public function getGeo()
    {
        return $this->geo;
    }

    public function setGeo($geo): self
    {
        $this->geo = $geo;

        return $this;
    }

    public function getGoogleJson(): ?array
    {
        return $this->googleJson;
    }

    public function setGoogleJson(?array $googleJson): self
    {
        $this->googleJson = $googleJson;

        return $this;
    }

    public function getGoogleCode(): ?string
    {
        return $this->googleCode;
    }

    public function setGoogleCode(?string $googleCode): self
    {
        $this->googleCode = $googleCode;

        return $this;
    }

    public function getStreetNumber(): ?string
    {
        return $this->streetNumber;
    }

    public function setStreetNumber(?string $streetNumber): self
    {
        $this->streetNumber = $streetNumber;

        return $this;
    }

    public function getRoute(): ?string
    {
        return $this->route;
    }

    public function setRoute(?string $route): self
    {
        $this->route = $route;

        return $this;
    }

    public function getAdminLevel5(): ?string
    {
        return $this->adminLevel_5;
    }

    public function setAdminLevel5(?string $adminLevel_5): self
    {
        $this->adminLevel_5 = $adminLevel_5;

        return $this;
    }

    public function getAdminLevel4(): ?string
    {
        return $this->adminLevel_4;
    }

    public function setAdminLevel4(?string $adminLevel_4): self
    {
        $this->adminLevel_4 = $adminLevel_4;

        return $this;
    }

    public function getAdminLevel3(): ?string
    {
        return $this->adminLevel_3;
    }

    public function setAdminLevel3(?string $adminLevel_3): self
    {
        $this->adminLevel_3 = $adminLevel_3;

        return $this;
    }

    public function getAdminLevel2(): ?string
    {
        return $this->adminLevel_2;
    }

    public function setAdminLevel2(?string $adminLevel_2): self
    {
        $this->adminLevel_2 = $adminLevel_2;

        return $this;
    }

    public function getAdminLevel1(): ?string
    {
        return $this->adminLevel_1;
    }

    public function setAdminLevel1(?string $adminLevel_1): self
    {
        $this->adminLevel_1 = $adminLevel_1;

        return $this;
    }

    public function getCountry(): ?string
    {
        return $this->country;
    }

    public function setCountry(?string $country): self
    {
        $this->country = $country;

        return $this;
    }

    public function getPostal(): ?string
    {
        return $this->postal;
    }

    public function setPostal(?string $postal): self
    {
        $this->postal = $postal;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(?string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getLocationType(): ?string
    {
        return $this->locationType;
    }

    public function setLocationType(?string $locationType): self
    {
        $this->locationType = $locationType;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(?string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getImported(): ?bool
    {
        return $this->imported;
    }

    public function setImported(?bool $imported): self
    {
        $this->imported = $imported;

        return $this;
    }
}
