<?php

namespace App\Contracts;

use App\Entity\Place;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class GoogleMapsApiInterface
{
    private $client;
    private const ALLOWED_STATUS = ['OK', 'ZERO_RESULTS', 'INVALID_REQUEST'];
    /**
     * @var EntityManagerInterface
     */
    private $em;

    public function __construct(HttpClientInterface $client, EntityManagerInterface $em)
    {
        $this->client = $client;
        $this->em = $em;
    }

    public function reverse(string $token, Place $place)
    {
        $endpoint = 'https://maps.googleapis.com/maps/api/geocode/json?';
        $url = $endpoint . 'latlng=' . $place->getLatitude() . ',' . $place->getLongitude() . '&key=' . $token;
        $response = $this->client->request(
            'GET',
            $url
        );
        $content = $response->toArray();
        $status = $content['status'];

        if (in_array($content['status'], self::ALLOWED_STATUS, true)) {
            $globalCode = $content['plus_code']['global_code'];
            $result = $content['results'];
            $coreResult = [];

            if (0 < count($result)) {
                foreach ($result as $address) {
                    if ($address['geometry']['location_type'] === 'ROOFTOP' && empty($coreResult)) {
                        $coreResult = $address;
                    }

                    if (empty($coreResult)) {
                        $coreResult = $address;
                    }
                }
            }

            if ('ZERO_RESULTS' !== $status) {
                foreach ($coreResult['address_components'] as $data) {
                    // street number
                    if (in_array('street_number', $data['types'], true)) {
                        $place->setStreetNumber($data['long_name']);
                    }

                    // route
                    if (in_array('route', $data['types'], true)) {
                        $place->setRoute($data['long_name']);
                    }

                    // lvl 5
                    if (in_array('administrative_area_level_5', $data['types'], true)) {
                        $place->setAdminLevel5($data['long_name']);
                    }
                    // if lvl 5 is empty and there is a locality data
                    if (null === $place->getAdminLevel5()
                        && in_array('sublocality_level_3', $data['types'], true)
                    ) {
                        $place->setAdminLevel5($data['long_name']);
                    }

                    // lvl 4
                    if (in_array('administrative_area_level_4', $data['types'], true)) {
                        $place->setAdminLevel4($data['long_name']);
                    }
                    // if lvl 4 is empty and there is a locality data
                    if (null === $place->getAdminLevel4()
                        && in_array('sublocality_level_2', $data['types'], true)
                    ) {
                        $place->setAdminLevel4($data['long_name']);
                    }

                    // lvl 3
                    if (in_array('administrative_area_level_3', $data['types'], true)) {
                        $place->setAdminLevel3($data['long_name']);
                    }
                    // if lvl 3 is empty and there is a locality data
                    if (null === $place->getAdminLevel3()
                        && (in_array('sublocality', $data['types'], true)
                            || in_array('sublocality_level_1', $data['types'], true))
                    ) {
                        $place->setAdminLevel3($data['long_name']);
                    }

                    // lvl 2
                    if (in_array('administrative_area_level_2', $data['types'], true)) {
                        $place->setAdminLevel2($data['long_name']);
                    }
                    // if lvl 2 is empty and there is a locality data
                    if (null === $place->getAdminLevel2()
                        && in_array('locality', $data['types'], true)
                    ) {
                        $place->setAdminLevel2($data['long_name']);
                    }

                    // lvl 1
                    if (in_array('administrative_area_level_1', $data['types'], true)) {
                        $place->setAdminLevel1($data['long_name']);
                    }

                    // Country
                    if (in_array('country', $data['types'], true)) {
                        $place->setCountry($data['long_name']);
                    }

                    // postal code
                    if (in_array('postal_code', $data['types'], true)) {
                        $place->setPostal($data['long_name']);
                    }
                }
                $place->setAddress($coreResult['formatted_address']);
                $place->setLocationType($coreResult['geometry']['location_type']);
            }

            $place->setGoogleCode($globalCode);
            $place->setGoogleJson($content);
            $place->setStatus($status);
            $place->setImported(true);
            $this->em->persist($place);
            $this->em->flush();

            return ['imported' => true, 'status' => $status];
        }

        return ['imported' => false, 'status' => $status, 'error_message' => $content['error_message']];
    }
}
