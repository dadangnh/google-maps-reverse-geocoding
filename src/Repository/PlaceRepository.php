<?php

namespace App\Repository;

use App\Entity\Place;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\DBAL\DBALException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Place|null find($id, $lockMode = null, $lockVersion = null)
 * @method Place|null findOneBy(array $criteria, array $orderBy = null)
 * @method Place[]    findAll()
 * @method Place[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PlaceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Place::class);
    }

    /**
     * @param int $count
     * @param bool $unprocessed
     * @return array
     */
    public function getRandomDataAsObject(int $count, bool $unprocessed): array
    {
        if ($unprocessed) {
            $data = $this->getRandomUnprocessedData($count);
        } else {
            $data = $this->getRandomData($count);
        }
        $output = [];
        foreach ($data as $place) {
            $output[] = $this->findOneBy(['id' => $place['id']]);
        }
        return $output;
    }

    /**
     * @param int $count
     * @param bool $unprocessed
     * @return array
     */
    public function getRandomDataAsArray(int $count, bool $unprocessed): array
    {
        if ($unprocessed) {
            return $this->getRandomUnprocessedData($count);
        }

        return $this->getRandomData($count);
    }

    /**
     * @param int $count
     * @return array
     */
    private function getRandomData(int $count): array
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = '
            SELECT * FROM place p
            ORDER BY random()
            LIMIT :limit
        ';
        try {
            $stmt = $conn->prepare($sql);
        } catch (DBALException $e) {
        }
        try {
            $stmt->execute(['limit' => $count]);
        } catch (DBALException $e) {
        }
        return $stmt->fetchAll();
    }

    /**
     * @param int $count
     * @return array
     */
    private function getRandomUnprocessedData(int $count): array
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = '
            SELECT *
            FROM place p
            WHERE p.imported is not true
            ORDER BY random()
            LIMIT :limit
        ';
        try {
            $stmt = $conn->prepare($sql);
        } catch (DBALException $e) {
        }
        try {
            $stmt->execute([
                'limit' => $count
            ]);
        } catch (DBALException $e) {
        }
        return $stmt->fetchAll();
    }
}
