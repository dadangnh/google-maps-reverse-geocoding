<?php


namespace App\Helper;


use App\Entity\Place;
use RuntimeException;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Uid\Uuid;

/**
 * Class ConsoleHelper
 * @package App\Helper
 */
class ConsoleHelper
{
    /**
     * Method for checking numeric parameter
     * @param SymfonyStyle $io
     * @param float|null $param
     * @param string $question
     * @return int|string
     */
    public function checkNumericParameter(SymfonyStyle $io, ?float $param, string $question)
    {
        while (null === $param || '' === $param || !is_numeric($param)) {
            $param = $io->ask($question, null,  static function ($number) {
                if (!is_numeric($number)) {
                    throw new RuntimeException('You must type a coordinate.');
                }

                return (float) $number;
            });
        }

        return $param;
    }

    /**
     * Method for checking numeric parameter
     * @param SymfonyStyle $io
     * @param int|null $param
     * @param string $question
     * @return int|string
     */
    public function checkIntegerParameter(SymfonyStyle $io, ?int $param, string $question)
    {
        while (null === $param || '' === $param || !is_numeric($param)) {
            $param = $io->ask($question, null,  static function ($number) {
                if (!is_numeric($number)) {
                    throw new RuntimeException('You must type a valid integer number.');
                }

                return (int) $number;
            });
        }

        return $param;
    }

    /**
     * @param SymfonyStyle $io
     * @param string|null $id
     * @param string $question
     * @return mixed|string
     */
    public function checkUuidOnInputValid(SymfonyStyle $io, ?string $id, string $question)
    {
        while (null === $id || '' === $id) {
            $id = $io->ask($question, null, function ($string) {
                return $this->checkValidUid($string);
            });
        }

        return $id;
    }

    /**
     * Method to make data table row from array of object
     * @param array $data
     * @return array
     */
    public function makeDataRowFromObject(array $data): array
    {
        $counter = 0;
        $output = [];

        /** @var Place $place */
        foreach ($data as $place) {
            $counter++;
            $output[] = [
                $counter,
                $place->getId()->toString(),
                $place->getLatitude(),
                $place->getLongitude(),
                $place->getAdminLevel5(),
                $place->getAdminLevel4(),
                $place->getAdminLevel3(),
                $place->getAdminLevel2(),
                $place->getAdminLevel1(),
                $place->getCountry()
            ];
        }

        return $output;
    }

    /**
     * Method to make the output table
     * @param SymfonyStyle $io
     * @param array $data
     */
    public function makeOutputTableFromArrayData(SymfonyStyle $io, array $data): void
    {
        $io->text('Data location.');
        $io->table(
            [
                'No',
                'ID',
                'Latitude',
                'Longitude',
                'Admin Level 5',
                'Admin Level 4',
                'Admin Level 3',
                'Admin Level 2',
                'Admin Level 1',
                'Country',
            ],
            $data
        );
    }

    /**
     * @param $string
     * @return mixed
     */
    private function checkValidUid($string)
    {
        if (36 === strlen($string) && Uuid::isValid($string)) {
            return $string;
        }

        if (36 <= strlen($string) && preg_match('/,/', $string)) {
            $data = explode(',', $string);
            $dataCount = count($data);
            $dataCounter = 0;
            foreach ($data as $uid) {
                if (36 === strlen($uid) && Uuid::isValid($uid)) {
                    $dataCounter++;
                }
            }

            if ($dataCounter === $dataCount) {
                return $string;
            }

            throw new RuntimeException('You must input a valid Uuid.');
        }

        throw new RuntimeException('You must input a valid Uuid.');
    }
}
