<?php

namespace App\Helper;

use App\Entity\Place;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class PlaceHelper
 * @package App\Helper
 */
class PlaceHelper
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * PlaceHelper constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @param float $latitude
     * @param float $longitude
     * @return bool
     */
    public function insertLocation(float $latitude, float $longitude): bool
    {
        // Get Place Entity Repository and check whether data already on database
        $data = $this->em->getRepository('App:Place')
            ->findOneBy(['latitude' => $latitude, 'longitude' => $longitude]);

        // If no data available, make new data
        if (null === $data) {
            $place = new Place();
            $place->setLatitude($latitude);
            $place->setLongitude($longitude);
            $place->setGeography('SRID=4326;POINT(' . $latitude . ' ' . $longitude . ')');
            $place->setGeometry('POINT(' . $latitude . ' ' . $longitude . ')');
            $this->em->persist($place);
            $this->em->flush();

            return true;
        }

        return false;
    }
}
