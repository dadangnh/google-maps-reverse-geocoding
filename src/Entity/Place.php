<?php

namespace App\Entity;

use App\Repository\PlaceRepository;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\UuidInterface;
use Ramsey\Uuid\Doctrine\UuidGenerator;


/**
 * @ORM\Entity(repositoryClass=PlaceRepository::class)
 * @ORM\Table(name="place", indexes={
 *     @ORM\Index(name="idx_place_coordinate", columns={"id", "latitude", "longitude"}),
 *     @ORM\Index(name="idx_place_detail", columns={"admin_level5", "admin_level4", "admin_level3", "admin_level2", "admin_level1"}),
 *     @ORM\Index(name="idx_place_address", columns={"address"}),
 *     @ORM\Index(name="idx_place_import", columns={"id", "imported"}),
 * })
 */
class Place
{
    /**
     * @var UuidInterface
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\Column(type="uuid", unique=true)
     * @ORM\CustomIdGenerator(class=UuidGenerator::class)
     */
    private $id;

    /**
     * @ORM\Column(type="float")
     */
    private $latitude;

    /**
     * @ORM\Column(type="float")
     */
    private $longitude;

    /**
     * @ORM\Column(type="json", nullable=true)
     */
    private $googleJson = [];

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $googleCode;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $streetNumber;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $route;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $adminLevel5;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $adminLevel4;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $adminLevel3;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $adminLevel2;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $adminLevel1;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $country;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $postal;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $address;

    /**
     * @ORM\Column(type="string", length=32, nullable=true)
     */
    private $locationType;

    /**
     * @ORM\Column(type="string", length=16, nullable=true)
     */
    private $status;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $imported;

    /**
     * @ORM\Column(type="geography")
     */
    private $geography;

    /**
     * @ORM\Column(type="geometry")
     */
    private $geometry;

    public function getId(): UuidInterface
    {
        return $this->id;
    }

    public function getLatitude(): ?float
    {
        return $this->latitude;
    }

    public function setLatitude(float $latitude): self
    {
        $this->latitude = $latitude;

        return $this;
    }

    public function getLongitude(): ?float
    {
        return $this->longitude;
    }

    public function setLongitude(float $longitude): self
    {
        $this->longitude = $longitude;

        return $this;
    }

    public function getGoogleJson(): ?array
    {
        return $this->googleJson;
    }

    public function setGoogleJson(?array $googleJson): self
    {
        $this->googleJson = $googleJson;

        return $this;
    }

    public function getGoogleCode(): ?string
    {
        return $this->googleCode;
    }

    public function setGoogleCode(?string $googleCode): self
    {
        $this->googleCode = $googleCode;

        return $this;
    }

    public function getStreetNumber(): ?string
    {
        return $this->streetNumber;
    }

    public function setStreetNumber(?string $streetNumber): self
    {
        $this->streetNumber = $streetNumber;

        return $this;
    }

    public function getRoute(): ?string
    {
        return $this->route;
    }

    public function setRoute(?string $route): self
    {
        $this->route = $route;

        return $this;
    }

    public function getAdminLevel5(): ?string
    {
        return $this->adminLevel5;
    }

    public function setAdminLevel5(?string $adminLevel5): self
    {
        $this->adminLevel5 = $adminLevel5;

        return $this;
    }

    public function getAdminLevel4(): ?string
    {
        return $this->adminLevel4;
    }

    public function setAdminLevel4(?string $adminLevel4): self
    {
        $this->adminLevel4 = $adminLevel4;

        return $this;
    }

    public function getAdminLevel3(): ?string
    {
        return $this->adminLevel3;
    }

    public function setAdminLevel3(?string $adminLevel3): self
    {
        $this->adminLevel3 = $adminLevel3;

        return $this;
    }

    public function getAdminLevel2(): ?string
    {
        return $this->adminLevel2;
    }

    public function setAdminLevel2(?string $adminLevel2): self
    {
        $this->adminLevel2 = $adminLevel2;

        return $this;
    }

    public function getAdminLevel1(): ?string
    {
        return $this->adminLevel1;
    }

    public function setAdminLevel1(?string $adminLevel1): self
    {
        $this->adminLevel1 = $adminLevel1;

        return $this;
    }

    public function getCountry(): ?string
    {
        return $this->country;
    }

    public function setCountry(?string $country): self
    {
        $this->country = $country;

        return $this;
    }

    public function getPostal(): ?string
    {
        return $this->postal;
    }

    public function setPostal(?string $postal): self
    {
        $this->postal = $postal;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(?string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getLocationType(): ?string
    {
        return $this->locationType;
    }

    public function setLocationType(?string $locationType): self
    {
        $this->locationType = $locationType;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(?string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getImported(): ?bool
    {
        return $this->imported;
    }

    public function setImported(?bool $imported): self
    {
        $this->imported = $imported;

        return $this;
    }

    public function getGeography()
    {
        return $this->geography;
    }

    public function setGeography($geography): self
    {
        $this->geography = $geography;

        return $this;
    }

    public function getGeometry()
    {
        return $this->geometry;
    }

    public function setGeometry($geometry): self
    {
        $this->geometry = $geometry;

        return $this;
    }
}
