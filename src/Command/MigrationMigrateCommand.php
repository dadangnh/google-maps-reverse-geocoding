<?php

namespace App\Command;

use App\Entity\Place;
use App\Old\Entity\Koordinat;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Persistence\ObjectManager;
use RuntimeException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class MigrationMigrateCommand extends Command
{
    protected static $defaultName = 'migration:migrate';
    /**
     * @var ManagerRegistry
     */
    private $registry;

    /**
     * @var int
     */
    private $maximumProcessCount;
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * MigrationMigrateCommand constructor.
     * @param ManagerRegistry $registry
     * @param int $maximumProcessCount
     * @param EntityManagerInterface $em
     */
    public function __construct(ManagerRegistry $registry, int $maximumProcessCount, EntityManagerInterface $em)
    {
        parent::__construct();
        $this->registry = $registry;
        $this->maximumProcessCount = $maximumProcessCount;
        $this->em = $em;
    }

    /**
     * Main execution
     */
    protected function configure()
    {
        $this
            ->setDescription('Process migration from v1 to v2')
            ->addOption(
                'count',
                null,
                InputOption::VALUE_REQUIRED,
                'Number of data to be upgraded')
        ;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $count = $input->getOption('count');
        $dbOld = $this->registry->getManager('old');

        if (false === $dbOld->getRepository(Koordinat::class)->checkUpgradeColumnReady()) {
            $io->error('Old database is not ready for migration.');
            $io->text('To fix this, please run: symfony console migration:test');
            die;
        }

        // if user didn't give count number, ask for it
        while (null !== $count && ('' === $count || !is_numeric($count) || 0 === $count)) {
            $count = $io->ask('Please input number of data to be processed:', 10, static function ($number) {

                if (!is_numeric($number)) {
                    throw new RuntimeException('You must type a number. 0 is not allowed');
                }

                return (int) $number;
            });
        }

        if (null === $count) {
            $io->text('Process All');
            $totalData = $this->registry->getManager('old')
                ->getRepository(Koordinat::class)
                ->getTotalUnmigratedData()['count'];
            $batch = ceil($totalData / $this->maximumProcessCount);
            $counter = $this->processUpgrade($io, $batch, null);

        } else {
            $io->text(sprintf('Process %d data.', $count));
            $batch = ceil($count / $this->maximumProcessCount);
            $counter = $this->processUpgrade($io, $batch, $count);
        }

        $io->text(sprintf('Success upgrade %d data.', $counter));

        return Command::SUCCESS;
    }

    /**
     * @param SymfonyStyle $io
     * @param int $batch
     * @param int|null $count
     * @return int
     */
    private function processUpgrade(SymfonyStyle $io, int $batch, ?int $count): int
    {
        $counter = 0;
        $errorMessage = null;

        for ($i = 1; $i <= $batch; $i++) {
            $totalNumber = $this->maximumProcessCount;

            if ((null !== $count) && $count < $this->maximumProcessCount) {
                $totalNumber = $count;
            }

            $oldDb = $this->registry->getManager('old');
            $lists = $oldDb->getRepository(Koordinat::class)
                ->getListCanUpgrade($totalNumber);

            if (1 < $batch) {
                $io->text('Batch: ' . $i);
            }

            $io->progressStart($totalNumber);

            foreach ($lists as $list) {
                $counter = $this->doUpgrade($io, $list, $oldDb, $counter);
            }

            $io->progressFinish();
        }

        return $counter;
    }

    /**
     * @param SymfonyStyle $io
     * @param array $list
     * @param ObjectManager $oldDb
     * @param int $counter
     * @return int
     */
    private function doUpgrade(SymfonyStyle $io, array $list, ObjectManager $oldDb, int $counter): int
    {
        $counter++;
        /** @var Place $place */
        $place = $this->registry->getRepository(Place::class)
            ->findOneBy(['latitude' => $list['latitude'], 'longitude' => $list['longitude']]);

        if (null === $place) {
            $place = new Place();
            $this->saveEntity($io, $place, $list, $oldDb, 'new');
        } else {
            $this->saveEntity($io, $place, $list, $oldDb, null);
        }

        return $counter;
    }

    /**
     * @param SymfonyStyle $io
     * @param Place $place
     * @param array $list
     * @param ObjectManager $oldDB
     * @param string|null $process
     */
    private function saveEntity(SymfonyStyle $io, Place $place, array $list, ObjectManager $oldDB, ?string $process): void
    {
        if ('new' === $process) {
            $latitude = $list['latitude'];
            $longitude = $list['longitude'];
            $place->setLatitude($latitude);
            $place->setLongitude($longitude);
            $place->setGeography('SRID=4326;POINT(' . $latitude . ' ' . $longitude . ')');
            $place->setGeometry('POINT(' . $latitude . ' ' . $longitude . ')');
        }
        $place->setGoogleJson(json_decode($list['google_json'], true));
        $place->setGoogleCode($list['google_code']);
        $place->setStreetNumber($list['street_number']);
        $place->setRoute($list['route']);
        $place->setAdminLevel5($list['admin_level_5']);
        $place->setAdminLevel4($list['admin_level_4']);
        $place->setAdminLevel3($list['admin_level_3']);
        $place->setAdminLevel2($list['admin_level_2']);
        $place->setAdminLevel1($list['admin_level_1']);
        $place->setCountry($list['country']);
        $place->setPostal($list['postal']);
        $place->setAddress($list['address']);
        $place->setLocationType($list['location_type']);
        $place->setStatus($list['status']);
        $place->setImported($list['imported']);
        $this->em->persist($place);
        $this->em->flush();

        if ($oldDB->getRepository(Koordinat::class)->saveUpgraded($list['id'])) {
            $io->progressAdvance();
        }

        // dismiss object to save memory
        unset($place, $list, $latitude, $longitude);
    }
}
