<?php

namespace App\Command;

use App\Old\Entity\Koordinat;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class MigrationTestCommand extends Command
{
    protected static $defaultName = 'migration:test';
    /**
     * @var ManagerRegistry
     */
    private $registry;

    /**
     * MigrationTestCommand constructor.
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct();
        $this->registry = $registry;
    }

    /**
     * Docs
     */
    protected function configure()
    {
        $this
            ->setDescription('Test migration from version 1 to version 2')
        ;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $dbOld = $this->registry->getManager('old');

        $io->title('Connecting to old database');
        $io->text('Fetching data....');
        $totalData = $dbOld->getRepository(Koordinat::class)->findTotalData();
        $io->text(sprintf('Total data on old db: %d data.', $totalData['count']));

        $io->text('Showing random data from old db...');
        $randomData = $dbOld->getRepository(Koordinat::class)->findRandomData(10);
        $row = [];
        $counter = 0;
        foreach ($randomData as $data) {
            $counter++;
            $row[] = [
                $counter,
                $data['id'],
                $data['latitude'],
                $data['longitude'],
                $data['google_code'],
                $data['country'],
                $data['location_type'],
                $data['status']
            ];
        }

        $io->table(
            [
                'No',
                'ID',
                'Latitude',
                'Longitude',
                'Google Code',
                'Country',
                'Location Type',
                'Status'
            ],
            $row
        );
        $io->title('Checking if old table is ready to upgrade.');
        $needAlter = true;
        while ($needAlter) {
            $ready = $dbOld->getRepository(Koordinat::class)->checkUpgradeColumnReady();
            if ($ready) {
                $needAlter = false;
            }

            if ($needAlter) {
                $io->warning('Old table is not ready to upgrade.');
                $io->text('Preparing old table...');
                $alter = $dbOld->getRepository(Koordinat::class)->addUpgradeColumn();
                if (false !== $alter) {
                    $needAlter = false;
                    $io->text('Old table successfully altered');
                }
            }
        }

        $io->success('Old table is ready to be upgraded');
        $io->text('To process the upgrade, please run: symfony console migration:migrate');
        return Command::SUCCESS;
    }
}
