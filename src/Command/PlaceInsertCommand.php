<?php

namespace App\Command;

use App\Helper\ConsoleHelper;
use App\Helper\PlaceHelper;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class PlaceInsertCommand extends Command
{
    protected static $defaultName = 'place:insert';
    /**
     * @var EntityManagerInterface
     */
    private $em;
    /**
     * @var ConsoleHelper
     */
    private $consoleHelper;
    /**
     * @var PlaceHelper
     */
    private $placeHelper;

    /**
     * PlaceInsertCommand constructor.
     * @param ConsoleHelper $consoleHelper
     * @param PlaceHelper $placeHelper
     */
    public function __construct(ConsoleHelper $consoleHelper, PlaceHelper $placeHelper)
    {
        parent::__construct();
        $this->consoleHelper = $consoleHelper;
        $this->placeHelper = $placeHelper;
    }

    /**
     * Initialize the cli command information
     */
    protected function configure()
    {
        $this
            ->setDescription('Insert location data (latitude and longitude coordinate) to db')
            ->addOption('latitude', null, InputOption::VALUE_REQUIRED , 'latitude')
            ->addOption('longitude', null, InputOption::VALUE_REQUIRED, 'longitude')
        ;
    }

    /**
     * Execution method
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $latitude = $input->getOption('latitude');
        $longitude = $input->getOption('longitude');

        // Starting the process
        $io->title('Inserting Place Data');

        // Make sure that latitude and longitude always numeric
        $latitude = $this->consoleHelper
            ->checkNumericParameter($io, $latitude, 'Please input the latitude:');
        $longitude = $this->consoleHelper
            ->checkNumericParameter($io, $longitude, 'Please input the longitude:');

        // Give user information on latitude and longitude
        if ('' !== $latitude && '' !== $longitude) {
            $io->note(sprintf('You insert place with latitude: %s and longitude: %s', $latitude, $longitude));
        }

        // Convert to float in case format is not float
        $latitude = (float) $latitude;
        $longitude = (float) $longitude;

        if (is_float($latitude) && is_float($longitude)) {
            // Process insert to database
            if ($this->placeHelper->insertLocation($latitude, $longitude)) {
                $io->success('Success Import');
            } else {
                $io->warning('Already exist on database');
            }
        } else {
            $io->error('Please only use coordinate');
        }

        return Command::SUCCESS;
    }

}
