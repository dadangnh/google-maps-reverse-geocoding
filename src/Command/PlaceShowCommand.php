<?php

namespace App\Command;

use App\Entity\Place;
use App\Helper\ConsoleHelper;
use App\Helper\PlaceHelper;
use Doctrine\ORM\EntityManagerInterface;
use RuntimeException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Class PlaceShowCommand
 * @package App\Command
 */
class PlaceShowCommand extends Command
{
    protected static $defaultName = 'place:show';
    private $em;
    /**
     * @var ConsoleHelper
     */
    private $consoleHelper;
    /**
     * @var PlaceHelper
     */
    private $placeHelper;

    /**
     * PlaceShowCommand constructor.
     * @param EntityManagerInterface $em
     * @param ConsoleHelper $consoleHelper
     * @param PlaceHelper $placeHelper
     */
    public function __construct(EntityManagerInterface $em, ConsoleHelper $consoleHelper, PlaceHelper $placeHelper)
    {
        parent::__construct();
        $this->em = $em;
        $this->consoleHelper = $consoleHelper;
        $this->placeHelper = $placeHelper;
    }

    /**
     * Initialize the cli command information
     */
    protected function configure()
    {
        $this
            ->setDescription('Show coordinate data.')
            ->addOption(
                'count',
                null,
                InputOption::VALUE_REQUIRED,
                'How many data to be displayed')
            ->addOption(
                'random',
                null,
                InputOption::VALUE_NONE,
                'Set random output')
            ->addOption(
                'id',
                null,
                InputOption::VALUE_REQUIRED,
                'Set the ID to be displayed (can be multiple id separated by comma)')
        ;
    }

    /**
     * Execution method
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $count = $input->getOption('count');
        $random = $input->getOption('random');
        $id = $input->getOption('id');
        $action = null;

        // If no random option given and no id given, ask user for specific action needed
        if (!$random && (null === $id || '' === $id)) {
            // summon helper
            $helper = $this->getHelper('question');

            // create question
            $question = new ChoiceQuestion(
                'Please select what you want to see?',
                [
                    'random' => 'Random data (default)',
                    'specific_coordinate' => 'Specific by coordinate',
                    'specific_id' => 'Specific by id data (can be multiple separated by comma)'
                ],
                0
            );

            // handler for invalid response
            $question->setErrorMessage('Response %s is invalid');

            // set answer as action
            $action = $helper->ask($input, $output, $question);
            $io->text(sprintf('You just select: %s', $action));

            // if random selected, go to random action
            if ('random' === $action) {
                $random = true;
            }
        } elseif (null !== $id && '' !== $id) {
            $action = 'specific_id';
        }

        // for random action
        if ($random) {
            // if user didn't give count number, ask for it
            while (null === $count || '' === $count || !is_numeric($count)) {
                $count = $io->ask('Please input number of data to be displayed:', 10, static function ($number) {
                    if (!is_numeric($number)) {
                        throw new RuntimeException('You must type a number.');
                    }

                    return (int) $number;
                });
            }

            // make header
            $io->title('Show Data');
            $io->text(sprintf('Getting %d data', $count));

            // show as table
            if (1000 >= $count) {
                // get data as array
                $data = $this->em->getRepository('App:Place')
                    ->getRandomDataAsArray($count, false);
                $output = [];
                $counter = 0;

                // make table array from result
                foreach ($data as $list) {
                    $counter++;
                    $output[] = [
                        $counter,
                        $list['id'],
                        $list['latitude'],
                        $list['longitude'],
                        $list['admin_level5'],
                        $list['admin_level4'],
                        $list['admin_level3'],
                        $list['admin_level2'],
                        $list['admin_level1'],
                        $list['country']
                    ];
                }

                // create the output
                $this->consoleHelper->makeOutputTableFromArrayData($io, $output);

            // For more than 1000 result, use stream
            } else {
                // Make table section
                $section = $output->section();
                $table = new Table($section);

                // Make table header
                $table->addRow([
                    'No',
                    'ID                                      ',
                    'Latitude    ',
                    'Longitude   ',
                    'Admin Level 5',
                    'Admin Level 4',
                    'Admin Level 3',
                    'Admin Level 2',
                    'Admin Level 1',
                    'Country',
                ]);
                $table->render();
                $data = $this->em->getRepository('App:Place')
                    ->getRandomDataAsObject($count, false);

                // Print the data
                $counter = 0;
                /** @var Place $list */
                foreach ($data as $list) {
                    $counter++;
                    echo $counter . "\t"
                        . $list->getId() . "\t"
                        . $list->getLatitude() . "\t"
                        . $list->getLongitude() . "\t"
                        . $list->getAdminLevel5() . "\t"
                        . $list->getAdminLevel4() . "\t"
                        . $list->getAdminLevel3() . "\t"
                        . $list->getAdminLevel2() . "\t"
                        . $list->getAdminLevel1() . "\t"
                        . $list->getCountry() . PHP_EOL;
                }
            }

        // For specific coordinate
        } else if ('specific_coordinate' === $action) {
            // Make sure that latitude and longitude always numeric
            $latitude = $longitude = null;
            // get latitude and longitude data from user
            $latitude = $this->consoleHelper
                ->checkNumericParameter($io, $latitude, 'Please input the latitude:');
            $longitude = $this->consoleHelper
                ->checkNumericParameter($io, $longitude, 'Please input the longitude:');

            /** @var Place $data */
            $data = $this->em->getRepository('App:Place')
                ->findOneBy(['latitude' => $latitude, 'longitude' => $longitude]);

            // If coordinate is not available on database, insert it
            $result = null;
            if (null === $data) {
                $io->text('Data not found on database.');
                $io->text('Inserting data to database');

                // Process insert to database
                $io->progressStart(1);

                // If success insert, get the data
                if ($this->placeHelper->insertLocation($latitude, $longitude)) {
                    $io->progressAdvance();

                    /** @var Place $data */
                    $data = $this->em->getRepository('App:Place')
                        ->findOneBy(['latitude' => $latitude, 'longitude' => $longitude]);

                    $result = $this->consoleHelper->makeDataRowFromObject([$data]);

                } else {
                    $io->warning('Already exist on database');
                }

                $io->progressFinish();

            } else {
                $result = $this->consoleHelper->makeDataRowFromObject([$data]);
            }

            // Make the table
            $this->consoleHelper->makeOutputTableFromArrayData($io, $result);

        } elseif ('specific_id' === $action) {
            // if user didn't give count number, ask for it
            $id = $this->consoleHelper->checkUuidOnInputValid($io, $id, 'Please input uuid data: ');

            // If only one id specified
            if (36 === strlen($id)) {
                $data = $this->em->getRepository('App:Place')
                    ->findOneBy(['id' => $id]);
                // Make the table
                $this->consoleHelper
                    ->makeOutputTableFromArrayData($io, $this->consoleHelper->makeDataRowFromObject([$data]));

            } elseif (36 <= strlen($id)) {
                $data = $this->em->getRepository('App:Place')
                    ->findBy(['id' => explode(',', $id)]);

                // Make the table
                $this->consoleHelper
                    ->makeOutputTableFromArrayData($io, $this->consoleHelper->makeDataRowFromObject($data));
            }
        }

        return Command::SUCCESS;
    }
}
