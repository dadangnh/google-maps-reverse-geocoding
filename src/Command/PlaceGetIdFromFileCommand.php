<?php

namespace App\Command;

use App\Entity\Place;
use App\Helper\PlaceHelper;
use Doctrine\ORM\EntityManagerInterface;
use League\Csv\Exception;
use League\Csv\Reader;
use League\Csv\Statement;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Filesystem\Filesystem;

class PlaceGetIdFromFileCommand extends Command
{
    protected static $defaultName = 'place:get-id-from-file';
    /**
     * @var Filesystem
     */
    private $filesystem;
    /**
     * @var EntityManagerInterface
     */
    private $em;
    /**
     * @var PlaceHelper
     */
    private $placeHelper;

    public function __construct(Filesystem $filesystem, EntityManagerInterface $em, PlaceHelper $placeHelper)
    {
        parent::__construct();
        $this->filesystem = $filesystem;
        $this->em = $em;
        $this->placeHelper = $placeHelper;
    }

    protected function configure()
    {
        $this
            ->setDescription('Print data ID from file')
            ->addArgument('path', InputArgument::REQUIRED, 'Path file')
            ->addOption(
                'format',
                null,
                InputOption::VALUE_REQUIRED,
                'Format output [sql, cli]')
            ->addOption(
                'without-header',
                null,
                InputOption::VALUE_NONE,
                'File format without header (first row is data)')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $path = $input->getArgument('path');
        $format = $input->getOption('format');
        $header = $input->getOption('without-header');

        // Make sure the path exist
        while (true !== $this->filesystem->exists($path)) {
            $io->error('File doesnt exist or cannot read file');
            $path = $io->ask('Please insert your file location:');
        }

        while (!in_array($format, ['cli', 'sql'], true) || null === $format) {
            $format = $io->ask('Please insert a valid format [sql, cli]');
        }

        // Read the file as csv
        $file = Reader::createFromStream(fopen($path, 'rb'));

        $arrayId = [];
        if ($header) {
            // create statement
            $statement = new Statement();
            // Get the data
            $data = $statement->process($file);

            foreach ($data->getRecords() as [$latitude, $longitude]) {
                $id = $this->getIdFromLatLong($latitude, $longitude);
                if (null !== $id) {
                    $arrayId[] = $this->getIdFromLatLong($latitude, $longitude);
                }
            }
        } else {
            // Set the first row as header
            try {
                $file->setHeaderOffset(0);
            } catch (Exception $e) {
                $io->error('Failed to open file');
                die;
            }
            $head = $file->getHeader();

            // Normally, data format is [latitude,longitude] => latitude index no 0, longitude no 1
            $latitudeIndex = 0;
            $longitudeIndex = 1;

            // Looking the column formation
            // Get the header position index
            foreach ($head as $key => $value) {
                if ('latitude' === strtolower($value)) {
                    $latitudeIndex = $key;
                }
                if ('longitude' === strtolower($value)) {
                    $longitudeIndex = $key;
                }
            }

            foreach ($file->getRecords() as $row) {
                $latitude = (float) $row[$head[$latitudeIndex]];
                $longitude = (float) $row[$head[$longitudeIndex]];

                $id = $this->getIdFromLatLong($latitude, $longitude);
                if (null !== $id) {
                    $arrayId[] = $this->getIdFromLatLong($latitude, $longitude);
                }
            }
        }


        if ('cli' === $format) {
            $ids = implode(',', $arrayId);
        } else {
            $ids = '';
            foreach ($arrayId as $id) {
                $ids .= "'" . $id . "',";
            }
        }
        $io->title('List ID:');
        $io->text($ids);

        return Command::SUCCESS;
    }

    private function getIdFromLatLong($latitude, $longitude)
    {
        /** @var Place $place */
        $place = $this->em->getRepository('App:Place')
            ->findOneBy(['latitude' => $latitude, 'longitude' => $longitude]);
        if (null === $place) {
            if ($this->placeHelper->insertLocation($latitude, $longitude)) {
                /** @var Place $place */
                $place = $this->em->getRepository('App:Place')
                    ->findOneBy(['latitude' => $latitude, 'longitude' => $longitude]);
                return $place->getId()->toString();
            }

            return null;
        }

        return $place->getId()->toString();
    }
}
