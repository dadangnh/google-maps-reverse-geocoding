<?php

namespace App\Command;

use App\Contracts\GoogleMapsApiInterface;
use App\Helper\PlaceHelper;
use Doctrine\ORM\EntityManagerInterface;
use League\Csv\Exception;
use League\Csv\Reader;
use League\Csv\Statement;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Filesystem\Filesystem;

/**
 * Class PlaceImportCommand
 * @package App\Command
 */
class PlaceImportCommand extends Command
{
    protected static $defaultName = 'place:import';
    /**
     * @var EntityManagerInterface
     */
    private $em;
    /**
     * @var GoogleMapsApiInterface
     */
    private $googleMapsApi;
    /**
     * @var PlaceHelper
     */
    private $placeHelper;
    /**
     * @var Filesystem
     */
    private $filesystem;

    /**
     * PlaceImportCommand constructor.
     * @param EntityManagerInterface $em
     * @param GoogleMapsApiInterface $googleMapsApi
     * @param PlaceHelper $placeHelper
     * @param Filesystem $filesystem
     */
    public function __construct(EntityManagerInterface $em,
                                GoogleMapsApiInterface $googleMapsApi,
                                PlaceHelper $placeHelper,
                                Filesystem $filesystem)
    {
        parent::__construct();
        $this->em = $em;
        $this->googleMapsApi = $googleMapsApi;
        $this->placeHelper = $placeHelper;
        $this->filesystem = $filesystem;
    }

    /**
     * Initialize the cli command information
     */
    protected function configure()
    {
        $this
            ->setDescription('Import location data (latitude and longitude coordinate) from file')
            ->addArgument('path', InputArgument::REQUIRED, 'Path file')
            ->addOption(
                'without-header',
                null,
                InputOption::VALUE_NONE,
                'Use this if your file doesnt use header. Data format: latitude,longitude')
            ->addOption(
                'reverse',
                null,
                InputOption::VALUE_NONE,
                'Use this option to import and process geocoding to Google API')
            ->addOption(
                'token',
                null,
                InputOption::VALUE_REQUIRED,
                'Google API Token')
        ;
    }

    /**
     * Execution method
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $path = $input->getArgument('path');
        $header = $input->getOption('without-header');
        $reverse = $input->getOption('reverse');
        $token = $input->getOption('token');

        if ($reverse) {
            while (null === $token || '' === $token) {
                $token = $io->ask('Please provide token to connect to Google API: ');
            }
        }
        // Starting the process
        $io->title('Place Importer');

        // Make sure the path exist
        while (true !== $this->filesystem->exists($path)) {
            $io->error('File doesnt exist or cannot read file');
            $path = $io->ask('Please insert your file location:');
        }

        // Read the file as csv
        $file = Reader::createFromStream(fopen($path, 'rb'));

        // If user choose without header
        if ($header) {
            // create statement
            $statement = new Statement();
            // Get the data
            $data = $statement->process($file);

            // Start the progress
            $dataCount = count($data);
            $io->progressStart($dataCount);
            $counter = 0;
            $errorMessage = [];

            // Process each row
            foreach ($data->getRecords() as $value) {
                // Insert to database
                if ($this->placeHelper->insertLocation((float) $value[0], (float) $value[1])) {
                    if ($reverse) {
                        $place = $this->em->getRepository('App:Place')
                            ->findOneBy(['latitude' => $value[0], 'longitude' => $value[1]]);
                        $processReverse = $this->googleMapsApi->reverse($token, $place);
                        if ($processReverse['imported']) {
                            $counter++;
                            $io->progressAdvance();
                        } else if (!in_array($processReverse['error_message'], $errorMessage, true)) {
                            $errorMessage[] = $processReverse['error_message'];
                        }
                    } else {
                        $counter++;
                        $io->progressAdvance();
                    }
                }
            }
        } else {
            // Set the first row as header
            try {
                $file->setHeaderOffset(0);
            } catch (Exception $e) {
                $io->error('Failed to open file');
                die;
            }

            // Get the header data
            $dataCount = count($file);
            $head = $file->getHeader();

            // Normally, data format is [latitude,longitude] => latitude index no 0, longitude no 1
            $latitudeIndex = 0;
            $longitudeIndex = 1;

            // Looking the column formation
            // Get the header position index
            foreach ($head as $key => $value) {
                if ('latitude' === strtolower($value)) {
                    $latitudeIndex = $key;
                }
                if ('longitude' === strtolower($value)) {
                    $longitudeIndex = $key;
                }
            }

            // Start the importer
            $io->progressStart($dataCount);
            $counter = 0;
            $errorMessage = [];

            // Get data per row
            foreach ($file->getRecords() as $row) {
                // Insert to database based on header index position
                if ($this->placeHelper->insertLocation(
                    (float) $row[$head[$latitudeIndex]],
                    (float) $row[$head[$longitudeIndex]])
                ) {
                    if ($reverse) {
                        $place = $this->em->getRepository('App:Place')
                            ->findOneBy([
                                'latitude' => (float) $row[$head[$latitudeIndex]],
                                'longitude' => (float) $row[$head[$longitudeIndex]]]);
                        $processReverse = $this->googleMapsApi->reverse($token, $place);
                        if ($processReverse['imported']) {
                            $counter++;
                            $io->progressAdvance();
                        } elseif (!in_array($processReverse['error_message'], $errorMessage, true)) {
                            $errorMessage[] = $processReverse['error_message'];
                        }
                    } else {
                        $counter++;
                        $io->progressAdvance();
                    }
                }
            }
        }

        // Show finished progress
        $io->progressFinish();
        if (!empty($errorMessage)) {
            $io->table(['Error'], [$errorMessage]);
        }
        $io->success(sprintf("
Statistics:\n
Total number of data imported: %d\n
Total data already on database: %d\n
------------------------------------
Total %d data processed.
            ", $counter, ($dataCount - $counter) , $dataCount));

        return Command::SUCCESS;
    }
}
