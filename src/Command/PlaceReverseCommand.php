<?php

namespace App\Command;

use App\Contracts\GoogleMapsApiInterface;
use App\Entity\Place;
use App\Helper\ConsoleHelper;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Console\Style\SymfonyStyle;

class PlaceReverseCommand extends Command
{
    protected static $defaultName = 'place:reverse';
    private const ALLOWED_STATUS = ['OK', 'ZERO_RESULTS', 'INVALID_REQUEST'];
    private $em;
    /**
     * @var GoogleMapsApiInterface
     */
    private $googleMapsApi;
    /**
     * @var int
     */
    private $maximumProcessCount;

    public function __construct(EntityManagerInterface $em,
                                GoogleMapsApiInterface $googleMapsApi,
                                int $maximumProcessCount)
    {
        parent::__construct();
        $this->em = $em;
        $this->googleMapsApi = $googleMapsApi;
        $this->maximumProcessCount = $maximumProcessCount;
    }

    protected function configure()
    {
        $this
            ->setDescription('Reverse coordinate by id or by latitude and longitude')
            ->addOption(
                'count',
                null,
                InputOption::VALUE_REQUIRED,
                'How many data to be processed (randomly)')
            ->addOption(
                'id',
                null,
                InputOption::VALUE_REQUIRED,
                'Set the ID to be processed (can be multiple id separated by comma)')
            ->addOption(
                'token',
                null,
                InputOption::VALUE_REQUIRED,
                'Google API Token')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $consoleHelper = new ConsoleHelper();
        $token = $input->getOption('token');
        $id = $input->getOption('id');
        $count = $input->getOption('count');
        $action = null;

        while (null === $token || '' === $token) {
            $token = $io->ask('Please provide token to connect to Google API: ');
        }

        if ((null === $id || '' === $id) && (null === $count || '' === $count)) {
            // summon helper
            $helper = $this->getHelper('question');

            // create question
            $question = new ChoiceQuestion(
                'Please select what you want to process?',
                [
                    'random' => 'Random data (default)',
                    'specific_id' => 'Specific by id data (can be multiple separated by comma)'
                ],
                0
            );

            // handler for invalid response
            $question->setErrorMessage('Response %s is invalid');

            // set answer as action
            $action = $helper->ask($input, $output, $question);
            $io->text(sprintf('You just select: %s', $action));
        }

        if ('random' === $action || (null !== $count && '' !== $count)) {
            $count = $consoleHelper->checkIntegerParameter(
                $io,
                $count,
                'Please specify the number to be processed'
            );

            $batch = 1;
            if ($count > $this->maximumProcessCount) {
                $batch = ceil($count / $this->maximumProcessCount);
            }

            $io->title('Starting reverse');
            $counter = 0;
            for ($i = 1; $i <= $batch; $i++) {
                $totalNumber = $this->maximumProcessCount;
                if ($count < $this->maximumProcessCount) {
                    $totalNumber = $count;
                }
                $lists = $this->em->getRepository('App:Place')
                    ->getRandomDataAsObject($totalNumber, true);

                if (1 < $batch) {
                    $io->text('Batch: ' . $i);
                }
                $io->progressStart($totalNumber);
                [$counter, $errorMessage] = $this->processReverse($io, $lists, $token, $counter);
                $io->progressFinish();
            }
            if (!empty($errorMessage)) {
                $io->table(['Error'], [$errorMessage]);
            }
            $io->text(sprintf('Total %d data processed.', $counter));
        } elseif ('specific_id' === $action || (null !== $id && '' !== $id)) {
            $id = $consoleHelper->checkUuidOnInputValid($io, $id, 'Please input a valid id:');

            // If only one id specified
            if (36 === strlen($id)) {
                $place = $this->em->getRepository('App:Place')
                    ->findOneBy(['id' => $id]);
                if (null !== $place) {
                    $processReverse = $this->googleMapsApi->reverse($token, $place);
                    if ($processReverse['imported']) {
                        $data = $this->em->getRepository('App:Place')
                            ->findOneBy(['id' => $id]);
                        // Make the table
                        $consoleHelper->makeOutputTableFromArrayData($io,
                            $consoleHelper->makeDataRowFromObject([$data]));
                        $io->success(sprintf('Success processing %d data.', 1));
                    } else {
                        $io->error('Failed to process reverse');
                    }
                }

            } elseif (36 <= strlen($id)) {
                $data = $this->em->getRepository('App:Place')
                    ->findBy(['id' => explode(',', $id)]);

                $io->title('Starting reverse');
                $io->progressStart(count($data));

                $counter = 0;
                [$counter, $errorMessage] = $this->processReverse($io, $data, $token, $counter);
                $io->progressFinish();
                if (!empty($errorMessage)) {
                    $io->table(['Error'], [$errorMessage]);
                }
                $io->text(sprintf('Total %d data processed.', $counter));
            }

        }

        return Command::SUCCESS;
    }

    /**
     * @param SymfonyStyle $io
     * @param array $data
     * @param string $token
     * @param int $counter
     * @return array
     */
    private function processReverse(SymfonyStyle $io, array $data, string $token, int $counter): array
    {
        $errorMessage = [];
        /** @var Place $place */
        foreach ($data as $place) {
            if (!in_array($place->getStatus(), self::ALLOWED_STATUS, true)) {
                $processReverse = $this->googleMapsApi->reverse($token, $place);
                if ($processReverse['imported']) {
                    $counter++;
                    $io->progressAdvance();
                } else if (!in_array($processReverse['error_message'], $errorMessage, true)) {
                    $errorMessage[] = $processReverse['error_message'];
                }
            }
        }

        return [$counter, $errorMessage];
    }
}
