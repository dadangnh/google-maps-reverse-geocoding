<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200831155345 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE place (id UUID NOT NULL, latitude DOUBLE PRECISION NOT NULL, longitude DOUBLE PRECISION NOT NULL, google_json JSON DEFAULT NULL, google_code VARCHAR(255) DEFAULT NULL, street_number VARCHAR(255) DEFAULT NULL, route VARCHAR(255) DEFAULT NULL, admin_level5 VARCHAR(255) DEFAULT NULL, admin_level4 VARCHAR(255) DEFAULT NULL, admin_level3 VARCHAR(255) DEFAULT NULL, admin_level2 VARCHAR(255) DEFAULT NULL, admin_level1 VARCHAR(255) DEFAULT NULL, country VARCHAR(255) DEFAULT NULL, postal VARCHAR(255) DEFAULT NULL, address TEXT DEFAULT NULL, location_type VARCHAR(32) DEFAULT NULL, status VARCHAR(16) DEFAULT NULL, imported BOOLEAN DEFAULT NULL, geography geography NOT NULL, geometry geometry NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX idx_place_coordinate ON place (id, latitude, longitude)');
        $this->addSql('CREATE INDEX idx_place_detail ON place (admin_level5, admin_level4, admin_level3, admin_level2, admin_level1)');
        $this->addSql('CREATE INDEX idx_place_address ON place (address)');
        $this->addSql('CREATE INDEX idx_place_import ON place (id, imported)');
        $this->addSql('CREATE INDEX idx_place_geo ON place (id, geography, geometry)');
        $this->addSql('COMMENT ON COLUMN place.id IS \'(DC2Type:uuid)\'');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP TABLE place');
    }
}
