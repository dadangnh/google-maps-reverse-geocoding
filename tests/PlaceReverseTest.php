<?php

namespace App\Tests;

use App\Contracts\GoogleMapsApiInterface;
use App\Entity\Place;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpClient\MockHttpClient;
use Symfony\Component\HttpClient\Response\MockResponse;

class PlaceReverseTest extends TestCase
{
    public function testReverseWithInvalidKey(): void
    {
        $place = new Place();
        $place->setLatitude(-7.7621732);
        $place->setLongitude(101.762531);
        $client = new MockHttpClient(new MockResponse(
            ['status' => 'OK', 'result' => []],
            ['status' => 'OK', 'result' => []])
        );
        $em = $this->createMock(EntityManagerInterface::class);

        /** @var GoogleMapsApiInterface $googleMapsInterface */
        $googleMapsInterface = $this->getMockBuilder(GoogleMapsApiInterface::class)
            ->setConstructorArgs([$client, $em])->getMock();

        $googleMapsInterface->reverse('test', $place);
        self::assertTrue(true);
    }
}
